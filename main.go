package main

import "bitbucket.org/sdwalsh/epij/tree"

func main() {
	treeTraversal()
}

func treeTraversal() {
	// Construct a tree for tree problems
	//             1
	//            / \
	//					 2	 3
	//		 					/	\
	//						 4	 6
	//						/
	//					 5
	t2 := tree.Tree{Val: 2, Left: nil, Right: nil}
	t5 := tree.Tree{Val: 5, Left: nil, Right: nil}
	t6 := tree.Tree{Val: 6, Left: nil, Right: nil}
	t4 := tree.Tree{Val: 4, Left: &t5, Right: nil}
	t3 := tree.Tree{Val: 3, Left: &t4, Right: &t6}
	root := tree.Tree{Val: 1, Left: &t2, Right: &t3}

	root.Preorder()
	root.Inorder()
	root.Postorder()
}
