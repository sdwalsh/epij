package tree

import "fmt"

// Tree is a recursive Tree data type
type Tree struct {
	Val   int
	Left  *Tree
	Right *Tree
}

///////////////////
// Tree bootcamp //
///////////////////

// Tree traversals //

// Preorder traverses the tree from root to left tree to right tree
/*
	https://en.wikipedia.org/wiki/Tree_traversal
	Check if the current node is empty or null.
	Display the data part of the root (or current node).
	Traverse the left subtree by recursively calling the pre-order function.
	Traverse the right subtree by recursively calling the pre-order function.
*/
func (t Tree) Preorder() {
	preorderhelper(&t)
	fmt.Println()
}

func preorderhelper(t *Tree) {
	if t == nil {
		return
	}
	fmt.Print(t.Val)
	preorderhelper(t.Left)
	preorderhelper(t.Right)
}

// Inorder traverses the tree from the left leaf backtracking to the root and over to the right
/*
	https://en.wikipedia.org/wiki/Tree_traversal
	Check if the current node is empty or null.
	Traverse the left subtree by recursively calling the in-order function.
	Display the data part of the root (or current node).
	Traverse the right subtree by recursively calling the in-order function.
*/
func (t Tree) Inorder() {
	inorderhelper(&t)
	fmt.Println()
}

func inorderhelper(t *Tree) {
	if t == nil {
		return
	}
	inorderhelper(t.Left)
	fmt.Print(t.Val)
	inorderhelper(t.Right)
}

// Postorder traverses the tree from the left subtree to the right subtree and finally the root
/*
	https://en.wikipedia.org/wiki/Tree_traversal
	Check if the current node is empty or null.
	Traverse the left subtree by recursively calling the post-order function.
	Traverse the right subtree by recursively calling the post-order function.
	Display the data part of the root (or current node).
*/
func (t Tree) Postorder() {
	postorderhelper(&t)
	fmt.Println()
}

func postorderhelper(t *Tree) {
	if t == nil {
		return
	}
	postorderhelper(t.Left)
	postorderhelper(t.Right)
	fmt.Print(t.Val)
}
